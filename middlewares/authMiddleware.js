const jwt = require('jsonwebtoken');
const JWT_SECRET = require('../config/db').secret;

module.exports = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(401).json({message: `No authorization header found`});
  }

  const token = header.split(' ')[1];

  if (!token) {
    return res.status(401).json({message: `No token found`});
  }

  jwt.verify(token, JWT_SECRET);
  next();
};

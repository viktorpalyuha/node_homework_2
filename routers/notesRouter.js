const express = require('express');
const router = new express.Router();

const noteControllers = require('../controllers/noteControllers');

router.get('/', noteControllers.getUserNotes);

router.post('/', noteControllers.addUserNote);

router.get('/:id', noteControllers.getNoteById);

router.put('/:id', noteControllers.updateNoteById);

router.patch('/:id', noteControllers.switchNoteFlagById);

router.delete('/:id', noteControllers.deleteNoteById);

module.exports = router;

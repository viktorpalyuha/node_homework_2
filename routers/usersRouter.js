const express = require('express');
const router = new express.Router();
const userControllers = require('../controllers/userControllers');


router.get('/me', userControllers.getUser);

router.delete('/me', userControllers.deleteUser);

router.patch('/me', userControllers.changePassword);
module.exports = router;

const express = require('express');
const router = new express.Router();
const userControllers = require('../controllers/userControllers');
const {validationMiddleware} = require('../middlewares/validationMiddleware');

router.post('/register', validationMiddleware, userControllers.register);

router.post('/login', userControllers.login);

module.exports = router;

module.exports = {
  db: process.env.DB_URL,
  secret: process.env.SECRET,
};

const jwt = require('jsonwebtoken');
const JWT_SECRET = require('../config/db').secret;

const Note = require('../models/noteModel').Note;

module.exports.getUserNotes = async (req, res) => {
  const authHeader = req.headers['authorization'];
  const {offset, limit} = req.query;
  const token = authHeader.split(' ')[1];
  const userData = jwt.verify(token, JWT_SECRET);

  const notes = await Note.find({userId: userData.id}, {__v: 0})
      .skip(+offset)
      .limit(+limit);

  if (!notes) {
    return res.status(400).json({
      message: 'No notes found for the user',
    });
  }

  res.status(200).json({
    notes,
  });
};

module.exports.addUserNote = async (req, res) => {
  const header = req.headers['authorization'];
  const token = header.split(' ')[1];

  const {text} = req.body;

  if (!text) {
    return res.status(400).json({
      message: 'Please, provide the text field',
    });
  }

  const userData = jwt.verify(token, JWT_SECRET);

  const newNote = new Note({
    userId: userData.id,
    text,
  });

  await newNote.save();

  res.status(200).json({
    message: 'Success',
  });
};

module.exports.getNoteById = async (req, res) => {
  const {id} = req.params;

  if (!id) {
    return res.status(400).json({
      message: 'Please, enter the id',
    });
  }

  const note = await Note.findOne({_id: id}, {__v: 0});

  if (!note) {
    return res.status(400).json({
      message: 'No note with such id found',
    });
  }

  res.status(200).json({
    note,
  });
};

module.exports.updateNoteById = async (req, res) => {
  const {id} = req.params;
  const {text} = req.body;

  if (!id || !text) {
    return res.status(400).json({
      message: 'Please, enter both id and text',
    });
  }

  try {
    await Note.findByIdAndUpdate({_id: id}, {text});
  } catch (error) {
    return res.status(400).json({
      message: 'No note with such id found',
    });
  }

  res.status(200).json({
    message: 'Success',
  });
};

module.exports.switchNoteFlagById = async (req, res) => {
  const {id} = req.params;

  if (!id) {
    return res.status(400).json({
      message: 'Please, enter the id',
    });
  }

  await Note.findOne({_id: id}, (err, note) => {
    if (err) {
      return res.status(400).json({
        message: 'No note with such id found',
      });
    }

    note.completed = !note.completed;
    note.save((err) => {
      if (err) {
        return res.status(500).json({
          message: 'Server error',
        });
      }
    });
  });

  res.status(200).json({
    message: 'Success',
  });
};

module.exports.deleteNoteById = async (req, res) => {
  const {id} = req.params;

  if (!id) {
    return res.status(400).json({
      message: 'Please, enter the id',
    });
  }

  try {
    await Note.findByIdAndDelete({_id: id});
  } catch (error) {
    return res.status(400).json({
      message: 'No note with such id found',
    });
  }

  res.status(200).json({
    message: 'Successs',
  });
};

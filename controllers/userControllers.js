const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const JWT_SECRET = require('../config/db').secret;

const User = require('../models/userModel').User;

module.exports.register = (req, res) => {
  const {username, password} = req.body;

  if (!username || !password) {
    return res.status(400).json({
      message: 'Please, enter both username and password',
    });
  }

  const newUser = new User({
    username,
    password,
  });

  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      return res.status(500).json({
        message: 'Server error',
      });
    }

    bcrypt.hash(newUser.password, salt, (err, hash) => {
      if (err) {
        return res.status(500).json({
          message: 'Server error',
        });
      }

      newUser.password = hash;
      newUser.save((err) => {
        if (err) {
          res.status(500).json({message: 'The user has not been added'});
        } else {
          res.status(200).json({message: 'Success'});
        }
      });
    });
  });
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;

  if (!username || !password) {
    return res.status(400).json({
      message: 'Please, enter both username and password',
    });
  }

  const user = await User.findOne({username});

  if (!user) {
    return res.status(400).json({
      message: 'No user with such username',
    });
  }

  const checkPassword = await bcrypt.compare(password, user.password);

  if (!checkPassword) {
    res.status(400).json({
      message: 'Wrong password',
    });
  } else {
    const token = jwt.sign({username: user.username, id: user._id}, JWT_SECRET);
    res.status(200).json({
      message: 'Success',
      jwt_token: token,
    });
  }
};

module.exports.getUser = async (req, res) => {
  const header = req.headers['authorization'];

  const token = header.split(' ')[1];

  const userData = jwt.verify(token, JWT_SECRET);

  const foundUser = await User.findById({_id: userData.id}, (err) => {
    if (err) {
      return res.status(500).json({
        message: 'Server error',
      });
    }
  });

  res.status(200).json({
    user: {
      _id: foundUser._id,
      username: foundUser.username,
      createdDate: foundUser.createdDate,
    },
  });
};

module.exports.deleteUser = async (req, res) => {
  const header = req.headers['authorization'];

  const token = header.split(' ')[1];

  const userData = jwt.verify(token, JWT_SECRET);

  await User.findByIdAndDelete({_id: userData.id}, (err) => {
    if (err) {
      return res.status(500).json({
        message: 'Server error',
      });
    }
  });

  res.status(200).json({
    message: 'Success',
  });
};

module.exports.changePassword = async (req, res) => {
  const header = req.headers['authorization'];
  const {oldPassword, newPassword} = req.body;
  const token = header.split(' ')[1];

  const userData = jwt.verify(token, JWT_SECRET);
  const user = await User.findOne({_id: userData.id});
  const checkPassword = await bcrypt.compare(oldPassword, user.password);

  if (!checkPassword) {
    return res.status(400).json({
      message: 'Wrong old password entered',
    });
  }

  const hashedPassword = await bcrypt.hash(newPassword, 10);

  await User.findByIdAndUpdate({_id: userData.id}, {password: hashedPassword},
      (err) => {
        if (err) {
          return res.status(500).json({
            message: 'Server error',
          });
        }
      });

  res.status(200).json({
    message: 'Success',
  });
};

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const morgan = require('morgan');

require('dotenv').config();
const dataBase = require('./config/db');
const authRouter = require('./routers/authRouter');
const notesRouter = require('./routers/notesRouter');
const usersRouter = require('./routers/usersRouter');
const authMiddleware = require('./middlewares/authMiddleware');

const app = express();

const port = process.env.PORT;

app.use(cors());
app.use(bodyParser.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);


app.use(authMiddleware);
app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);

mongoose.connect(dataBase.db, {useNewUrlParser: true,
  useUnifiedTopology: true}, () => {
  console.log('Connected to db');
  app.listen(port, async () => {
    console.log('Server is running and the port is ' + port);
  });
});
